﻿#include <iostream>
#include <stack>

using namespace std;

int main()
{
    setlocale(LC_CTYPE, "rus");
    int x;

    cout << "Введите размер структуры данных стек: ";

    cin >> x;
    int* p = new int [x];

    stack <int> fstack;
    cout << "Все элементы структуры данных стек: ";
    int i = 1;
    while (i <= x)
    {
        fstack.push (i);
        cout << i << " ";
        i++;
    }

    cout << "\n" << "Верхний элемент: " << fstack.top();
    cout << "\n" << "Удаление верхнего элемента";
    fstack.pop();
    cout << "\n" << "Новый верхний элемент: " << fstack.top();

}